import { Accordion, Button, Card, Container } from 'react-bootstrap'
import { ArrowDownCircleFill, Laptop, Book, ShieldExclamation, CashStack, House, EmojiWinkFill } from 'react-bootstrap-icons'
import ScrollToTop from '../components/scrollToTop'
import { useRef } from 'react'
import { RESUME_DOWNLOAD_URL } from '../constants/urls'
import ScrollAnimation from 'react-animate-on-scroll';

const Resume = () => {

  const workRef = useRef()
  const handleScrollDownClick = () => {
    workRef.current.scrollIntoView({ behavior: 'smooth' })
  }


  return (
    <>
      <section className="pageLead">
        <div className="header">
          <ScrollAnimation animateIn='flipInY'>
            <h1>Resume</h1>
            <br />
            <Button href={RESUME_DOWNLOAD_URL}
              variant="outline-success" className="downloadBtn">
              Download Resume</Button>
          </ScrollAnimation>
        </div>

        <div className="scroll-div">
          <div>
            <ArrowDownCircleFill className="scrollDown" onClick={handleScrollDownClick} color="#18d26e" size={48} />
          </div>
        </div>
      </section>

      { /* MAIN CONTENT */}
      <main>
        <span ref={workRef}></span>
        <section id="pageSection" className="pageSection" style={{ minHeight: "40vh" }}>

          {/* Scroll Button */}
          <ScrollToTop />

          <ScrollAnimation animateIn='zoomIn'
            animateOut='zoomOut'>
            <div id="resume">
              {/* WORK EXPERIENCE */}
              <div className="header">WORK EXPERIENCE</div>
              <h6>I have worked in Banking, Insurance, Forensic Research and IT Consulting.</h6>
              <Container fluid={'md'}>
                <Accordion>
                  <Card id="accordionCard">
                    <Card.Header className="cardHeader">
                      <Accordion.Toggle as={Button} variant="outline-success" eventKey="0" id="accordionBtn">
                        View Details
                      </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                      <Card.Body className="cardBody">

                        {/*WORK TIMELINE */}
                        <div className="timeline">
                          <div className="center-line">
                            {/* <a className="scroll-icon"><House size={24} /></a> */}
                            <i className="scroll-icon"><House size={24} /></i>
                          </div>
                          <div className="row row-1">
                            <section>
                              <i className="icon fas fa-home"> <Laptop size={24} /></i>
                              <div className="details">
                                <span className="title">Software Engineer</span>
                                <span className="date">2020-Present</span>
                              </div>
                              <p className="company"><em>Freelance, Ghana</em></p>
                              <p><ul>
                                <li> Designed and implemented a full-stack authentication program using Spring Boot, ReactJS and MySQL</li>
                                <li>Developed and published “Read’E”, an android app, on the Google play store.</li>
                                <li> Implemented frontend components for a Weather App using React JS.</li>
                              </ul>
                              </p>
                              {/* <div className="bottom">
                                     <a href="#">Read more</a>
                                     <i>- Someone famous</i>
                                  </div> */}
                            </section>
                          </div>
                          <div className="row row-2">
                            <section>
                              <i className="icon"><Book size={24} /></i>
                              <div className="details">
                                <span className="title">Research Assistant</span>
                                <span className="date">2019</span>
                              </div>
                              <p className="company"><em>Cybersecurity Centre, University of Hertfordshire</em></p>
                              <p><ul>
                                <li>Assisted in examining numerous data erasing tools and methods.</li>
                                <li>Conducted forensic lab experiments on physical drives using tools such as OSForensics and Autopsy.</li>
                              </ul>
                              </p>
                            </section>
                          </div>
                          <div className="row row-1">
                            <section>
                              <i className="icon"><ShieldExclamation size={24} /></i>
                              <div className="details">
                                <span className="title">Software Engineer</span>
                                <span className="date">2017-2018</span>
                              </div>
                              <p className="company"><em>Insurance Collections Bureau, UK</em></p>
                              <p>
                                <ul>
                                  <li>Migrated the company’s legacy finance software to a web-based application using JSPs, Servlets, MaterializeCSS, HTML5, and JavaScript.</li>
                                  <li>Utilized Apache POI library to mitigate the time used in the daily processing of customer debts by an average of 2 hours.</li>
                                  <li>Implemented frontend components for a Weather App using React JS.</li>
                                </ul>
                              </p>
                            </section>
                          </div>
                          <div className="row row-2">
                            <section>
                              <i className="icon"><CashStack size={24} /></i>
                              <div className="details">
                                <span className="title">MIS IT Support</span>
                                <span className="date">2015-2016</span>
                              </div>
                              <p className="company"><em><abbr title="Ghana Interbank Payment and Settlement Systems Limited">GHIPSS</abbr><span> ,</span>Ghana</em></p>
                              <p>
                                <ul>
                                  <li>Performed database uploads and backup of company data by utilizing HeidiSQL</li>
                                  <li>Assisted in the implementation and testing of software applications (such as Automated Clearing House Portal and the Mandate Exchange Portal) for the National Bankers
Clearing House.</li>
                                  <li>Supported the setting up of 3 new banks to the Clearing House System.</li>
                                </ul>
                              </p>
                            </section>
                          </div>
                          <div className="row row-1">
                            <section>
                              <i className="icon"><Laptop size={24} /></i>
                              <div className="details">
                                <span className="title">Mobile Developer - Part-time</span>
                                <span className="date">2015-2016</span>
                              </div>
                              <p className="company"><em>Bitberg Limited, Ghana</em></p>
                              <p>
                                <ul>
                                  <li>Led the mobile development of the 2016 CoC Easter Conference App which was accessed by 100+ users.</li>
                                  <li>Collaborated with other team members to perform requirement analysis and software design.</li>
                                </ul>
                              </p>
                            </section>
                          </div>
                          <div className="row row-2">
                            <section>
                              <i className="icon"><CashStack size={24} /></i>
                              <div className="details">
                                <span className="title">IT Intern</span>
                                <span className="date">2012</span>
                              </div>
                              <p className="company"><em>Bank of Ghana</em></p>
                              <p>
                                <ul>
                                  <li>Assisted in setting up new workstations for nationwide distribution.</li>
                                  <li>Supported in assembling and upgrading company hardware.</li>
                                </ul>
                              </p>
                            </section>
                          </div>
                        </div>

                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                </Accordion>
              </Container>

              <br />
              {/* EDUCATION */}
              <div className="header"> EDUCATION</div>
              <h6>Trust me when I say I attended the best schools <EmojiWinkFill color="gold" /></h6>
              <Container fluid={'md'}>
                <Accordion>
                  <Card id="accordionCard">
                    <Card.Header className="cardHeader">
                      <Accordion.Toggle as={Button} variant="outline-success" eventKey="0" id="accordionBtn">
                        View Details
                      </Accordion.Toggle>
                    </Card.Header>
                    <Accordion.Collapse eventKey="0">
                      <Card.Body className="cardBody">

                        {/*EDUCATION TIMELINE */}
                        <div className="timeline">
                          <div className="center-line">
                            <i className="scroll-icon"><House size={24} /></i>
                          </div>
                          <div className="row row-1">
                            <section>
                              <i className="icon fas fa-home"> <Book size={24} /></i>
                              <div className="details">
                                <span className="degree">MSc Software Engineering</span>
                                <span className="date">2017-2019</span>
                              </div>
                              <p className="company">University of Hertfordshire, UK</p>
                              <p>
                              </p>
                            </section>
                          </div>
                          <div className="row row-2">
                            <section>
                              <i className="icon"><Book size={24} /></i>
                              <div className="details">
                                <span className="degree">BSc Computer Engineering</span>
                                <span className="date">2011-2015</span>
                              </div>
                              <p className="company"><abbr title="Kwame Nkrumah University of Science and Technology">KNUST</abbr><span> ,</span> Ghana</p>
                              <p>
                              </p>
                            </section>
                          </div>
                          <div className="row row-1">
                            <section>
                              <i className="icon"><Book size={24} /></i>
                              <div className="details">
                                <span className="degree">Java SE and Android Programming Certificate</span>
                                <span className="date">2013</span>
                              </div>
                              <p className="company">Ghana-India Kofi Annan Centre of Excellence in ICT</p>
                              <p>

                              </p>
                            </section>
                          </div>
                          <div className="row row-2">
                            <section>
                              <i className="icon"><Book size={24} /></i>
                              <div className="details">
                                <span className="degree">High School WASSCE Certificate</span>
                                <span className="date">2007-2011</span>
                              </div>
                              <p className="company">Mfantsipim School, Ghana</p>
                              <p>
                              </p>
                            </section>
                          </div>
                        </div>

                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                </Accordion>
              </Container>

            </div>

          </ScrollAnimation>
        </section>

      </main>
    </>
  );
}

export default Resume;