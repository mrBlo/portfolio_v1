import React from 'react'
import { EmojiSmileFill } from 'react-bootstrap-icons'
import ContactForm from '../components/contactform'
import ScrollAnimation from 'react-animate-on-scroll';


export default function Contact() {


    return (
        <>
            <main style={{ backgroundColor: "#0e1e25" }}>
                <br />
                <section id="pageSection" className="pageSection" style={{ minHeight: "85vh" }}>
                    <ScrollAnimation animateIn='zoomIn'>
                        <h1 className="header">Contact Me</h1>
                        <h6 style={{ color: "white", marginBottom: "1rem" }}>Don't be a stranger <EmojiSmileFill color="gold" /></h6>
                        <ContactForm />
                    </ScrollAnimation>
                </section>

            </main>

        </>
    )
}
