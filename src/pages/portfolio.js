
import ScrollToTop from '../components/scrollToTop'
import { useRef } from 'react'
import { ArrowDownCircleFill, Link45deg } from 'react-bootstrap-icons'
import { Badge, Button, Container, Row } from 'react-bootstrap'
import { flickrImages } from '../components/flickrImages'
import Carousel from 'react-gallery-carousel';
import 'react-gallery-carousel/dist/index.css';
import ScrollAnimation from 'react-animate-on-scroll';

const Portfolio = () => {

  const titleRef = useRef()
  const handleScrollDownClick = () => {
    titleRef.current.scrollIntoView({ behavior: 'smooth' })
  }

  return (
    <>
      <section className="pageLead">
        <div className="header">
          <ScrollAnimation animateIn='flipInY'>
            <h1>Portfolio</h1>
          </ScrollAnimation>
        </div>

        <div className="scroll-div">
          <div>
            <ArrowDownCircleFill className="scrollDown" onClick={handleScrollDownClick} color="#18d26e" size={48} />
          </div>

        </div>
      </section>

      <main>
        <br />
        <span ref={titleRef}></span>
        { /* CONTENT */}
        <section id="pageSection" className="pageSection">

          {/* Scroll Button */}
          <ScrollToTop />

          <ScrollAnimation animateIn='fadeInLeft'
            animateOut='fadeOutLeft'>
            <div className="header"> SOFTWARE PORTFOLIO</div>
            <h6>These are some of my work</h6>
          </ScrollAnimation>

          <div className="softwareProjects">
            <Container fluid>
              <ScrollAnimation animateIn='zoomIn' duration='1.5'>
                <Row>
                  <div class="col-12 col-md-4">
                    <div class="mycard mb-3">
                      <div class="mycard-img" style={{ backgroundImage: "url(https://bitbucket.org/mrBlo/weather-app/raw/f7a716eb0a9c9f8b37eb67f4c14a26d466ac21e2/app_image.png)" }}>
                        <div class="mycard-img-overlay d-flex flex-column justify-content-between">
                          <h4 class="card-title">Weather App</h4>

                          <p class="card-text">
                            A web app which displays the weather forecast for user-specified locations. </p>
                          <Button className="button btn-success" href="https://weatherblo.netlify.app" target="_blank" rel="noopener noreferrer">
                            <Link45deg /> View Site
                        </Button>
                          <div className="projectBadges">
                            <Badge variant="primary" className="badge">REACT JS</Badge>
                            <Badge variant="secondary" className="badge">HTML</Badge>
                            <Badge variant="warning" className="badge">AXIOS</Badge>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-4">
                    <div class="mycard mb-3">
                      <div class="mycard-img" style={{ backgroundImage: "url(https://play-lh.googleusercontent.com/YT7k39jFoU2UAFCGd7YlGB1_PZ6DcwCnfBn7vIWWV9cr7YNK7DkLmxuJ6L5mouNthDQ=w3360-h1942-rw)" }}>
                        <div class="mycard-img-overlay d-flex flex-column justify-content-between">
                          <h4 class="card-title">Read'E</h4>
                          <p class="card-text">
                            An android News App serving the latest headlines and articles.</p>
                          <Button className="button btn-success" href="https://play.google.com/store/apps/details?id=com.blo.reade" target="_blank" rel="noopener noreferrer">
                            <Link45deg /> View Site
                        </Button>
                          <div className="projectBadges">
                            <Badge variant="primary" className="badge">JAVA</Badge>
                            <Badge variant="danger" className="badge">KOTLIN</Badge>
                            <Badge variant="success" className="badge">XML</Badge>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-4">
                    <div class="mycard mb-3">
                      <div class="mycard-img" style={{ backgroundImage: "url(https://bitbucket.org/mrBlo/clr-frontend/raw/079a41324bfa17e22fc228d29c89c24c9546aede/LoginPage.png)" }}>
                        <div class="mycard-img-overlay d-flex flex-column justify-content-between">
                          <h4 class="card-title">CLR</h4>
                          <p class="card-text">
                            A full-stack application that implements a Complete Login and Registration flow.
                        </p>
                          <Button className="button btn-success" href="https://myclr.netlify.app/" target="_blank" rel="noopener noreferrer">
                            <Link45deg /> View Site
                        </Button>
                          <div className="projectBadges">
                            <Badge variant="success" className="badge">SPRING BOOT</Badge>
                            <Badge variant="primary" className="badge">REDUX</Badge>
                            <Badge variant="light" className="badge">MYSQL</Badge>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Row>
              </ScrollAnimation>

              <ScrollAnimation animateIn='zoomIn' duration='1.5'>
                <Row>
                  <div class="col-12 col-md-4">
                    <div class="mycard mb-3">
                      <div class="mycard-img" style={{ backgroundImage: "url(https://bytebucket.org/mrBlo/profile-card/raw/5e44d7fbba0fb0b9587a23a4f7a58f27f5420c69/index_img.png)" }}>
                        <div class="mycard-img-overlay d-flex flex-column justify-content-between">
                          <h4 class="card-title">Afrifa's Card</h4>
                          <p class="card-text">
                            My personal online contact card
                        </p>
                          <Button className="button btn-success" href="https://isaacafrifa.netlify.app/" target="_blank" rel="noopener noreferrer">
                            <Link45deg /> View Site
                        </Button>
                          <div className="projectBadges">
                            <Badge variant="primary" className="badge">REACT JS</Badge>
                            <Badge variant="info" className="badge">BOOTSTRAP</Badge>
                            <Badge variant="dark" className="badge">HTML</Badge>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 col-md-4">
                    <div class="mycard mb-3">
                      <div class="mycard-img" style={{ backgroundImage: "url(https://bitbucket.org/mrBlo/congressapp/raw/fe04d52deeda962ffc223da34fe48bcdbeb2e699/app/src/main/res/drawable/congress.jpg)" }}>
                        <div class="mycard-img-overlay d-flex flex-column justify-content-between">
                          <h4 class="card-title">CoC Congress App</h4>
                          <p class="card-text">
                            The official 2016 Students Conference android app of the Church of Christ, Ghana
                        </p>
                          <Button className="button btn-success" href="https://tale.com/store/apps/details?id=com.bitberglimited.congress_app" target="_blank" rel="noopener noreferrer">
                            <Link45deg /> View Site
                        </Button>
                          <div className="projectBadges">
                            <Badge variant="primary" className="badge">JAVA</Badge>
                            <Badge variant="danger" className="badge">JSON</Badge>
                            <Badge variant="info" className="badge">GRADLE</Badge>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 col-md-4">
                    <div class="mycard mb-3">
                      <div class="mycard-img" style={{ backgroundImage: "url(https://bytebucket.org/mrBlo/portfolio/raw/50ae081936957492f4ae60f70d289e108f608002/app_image.png?token=76e2dd20c6439b4d793ac90253d2515e49597fab)" }}>
                        <div class="mycard-img-overlay d-flex flex-column justify-content-between">
                          <h4 class="card-title">Portfolio Site</h4>
                          <p class="card-text">
                            My personal portfolio website
                        </p>
                          <Button className="button btn-success" href="https://imafrifa.netlify.app/" target="_blank" rel="noopener noreferrer">
                            <Link45deg /> View Site
                        </Button>
                          <div className="projectBadges">
                            <Badge variant="primary" className="badge">REACT JS</Badge>
                            <Badge variant="danger" className="badge">BOOTSTRAP</Badge>
                            <Badge variant="info" className="badge">REACT SPRING</Badge>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Row>
              </ScrollAnimation>
            </Container>
          </div>



          <br /><br />
          <div className="photographyProjects">
            <ScrollAnimation animateIn='fadeInLeft'
              animateOut='fadeOutLeft'>
              <div className="header"> PHOTOGRAPHY PORTFOLIO</div>
              <h6>Some of my work</h6>
            </ScrollAnimation>

            <ScrollAnimation animateIn='zoomIn' duration='1.5'
              animateOut='zoomOut'>
              <Container fluid>
                <Carousel images={flickrImages} style={{ height: "60vh" }} hasThumbnails="false" isAutoPlaying="true" shouldMinimizeOnClick="true"
                  shouldMinimizeOnSwipeDown="true" shouldMaximizeOnClick="true" />
              </Container>
            </ScrollAnimation>
          </div>

        </section>

      </main>
    </>
  );
}

export default Portfolio;