import Image404 from "../assets/images/error-404-monochrome.svg";
import ScrollAnimation from 'react-animate-on-scroll';

const Page404 = () => {
    return (
        <>
            <section className="pageLead">
                <div className="header">
                    <ScrollAnimation animateIn='flipInY'>
                        <h3>Page Not Found</h3>
                        <img className="img-error fluid"
                            alt="404 avatar"
                            loading="lazy"
                            src={Image404} />
                    </ScrollAnimation>
                </div>
            </section>
        </>
    );
}

export default Page404;