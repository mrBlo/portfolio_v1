import { Container, Row, Col } from 'react-bootstrap'
import ReactTypingEffect from 'react-typing-effect'
import { Envelope, Linkedin, Github, Camera } from 'react-bootstrap-icons'
import ParticlesBg from 'particles-bg'
import ScrollAnimation from 'react-animate-on-scroll';

const Home = () => {

    return (
        <div className="home">
            {/*Particles Bg Component and Params */}
            <ParticlesBg num={50} type="cobweb" bg={true} />

            <Container fluid>
                <Row>
                    <Col>
                        <div className="heroText">
                            <ScrollAnimation animateIn='bounceInLeft' delay='200'>
                                <h1>Isaac Afrifa</h1>
                                </ScrollAnimation>
                                <ScrollAnimation animateIn='bounceInRight' delay='200'>
                                <h4>Software Engineer</h4>
                                </ScrollAnimation>
                                <ScrollAnimation animateIn='bounceInLeft' delay='200'>
                                <h5>I'm into <span> </span>
                                    <ReactTypingEffect
                                        text={[" Full-Stack Web.", " Android Dev.", " Photography."]}
                                        speed={200}
                                        typingDelay={100}
                                        eraseDelay={1000}
                                        eraseSpeed={100}
                                    />
                                </h5>
                                </ScrollAnimation>
                                <ScrollAnimation animateIn='zoomIn' delay='800'>
                                <div className="socials">
                                    <a className="icon-box" title="Mail" href="mailto:isaac.afrifa3@yahoo.com"><Envelope size={24} /></a>
                                    <a className="icon-box" title="LinkedIn" href="https://www.linkedin.com/in/isaac-afrifa-9aa543106"><Linkedin size={24} /></a>
                                    <a className="icon-box" title="Bitbucket" href="https://bitbucket.org/mrblo"><Github size={24} /></a>
                                    <a className="icon-box" title="Flickr" href="https://www.flickr.com/photos/afrifa/"><Camera size={24} /></a>
                                </div>
                                <br />
                            </ScrollAnimation>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default Home;