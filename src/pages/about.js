import { Container, Row, Col } from 'react-bootstrap'
import Avatar from '../assets/images/avatar.jpg'
import Tilt from 'react-parallax-tilt';
import { StarFill, Star, HeartFill } from 'react-bootstrap-icons'
import Chelsea from '../assets/images/chelsea.png'
import ScrollToTop from '../components/scrollToTop'
import ToolsTagCloud from '../components/toolsTagCloud'
import ScrollAnimation from 'react-animate-on-scroll';

const About = () => {

  return (
    <div className="about">

      <Container fluid>
        <div className="aboutMeSection">

          {/* Scroll Button */}
          <ScrollToTop />

          {/* About Me Section */}
          <ScrollAnimation animateIn='zoomIn' duration='1.5'>
            <Row>
              <Col lg={6}>
                <Tilt
                  className="tilt-img"
                  tiltMaxAngleX={35}
                  tiltMaxAngleY={35}
                  perspective={900}
                  scale={0.9}
                  glareEnable={true}
                  glareMaxOpacity={0.6}
                  transitionSpeed={2000}
                  gyroscope={true}>
                  <img src={Avatar} width="480" height="480" className="img-fluid avatar" alt="avatar" loading="lazy" />
                </Tilt>
              </Col>

              <Col lg={6} >
                <div className="aboutDetails">
                  <div className="header">
                    <h1>ABOUT ME</h1>
                  </div>

                  <p>My name is Isaac Mensah Afrifa but you can call me Nana Yaw.
                 I’m into building full-stack web apps, android development and photography.</p>
                  <p> I grew up in Ghana, where I had most of my education and have 5 years of experience working in product development in Ghana and the UK.
                  In addition to development, I am also a published Digital Forensic Researcher.</p>
                  <p className="lastParagraph"> I'm currently working as a Freelancer, developing both web and mobile apps, and consider myself an 'eternal student' who is
                     always looking for new ways to improve my skillset.</p>
                </div>
              </Col>
            </Row>
          </ScrollAnimation>
        </div>

        {/* SKILLS */}
        <div className="skills-section">
          <ScrollAnimation animateIn='zoomIn' duration='1.5'>
            <Row>
              <Col lg={6}>
                <h3>SKILLS</h3>
                <section id="skills" class="skills section-bg">
                  <div class="row skills-content">
                    <div class="col-md-12">

                      <div className="progress">
                        <span className="skill">Java <i className="val">80%</i></span>
                        <div className="progress-bar-wrap">
                          <div className="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style={{ width: "80%" }}></div>
                        </div>
                      </div>

                      <div className="progress">
                        <span className="skill">Javascript - React <i className="val">70%</i></span>
                        <div className="progress-bar-wrap">
                          <div className="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style={{ width: "70%" }}></div>
                        </div>
                      </div>

                      <div className="progress">
                        <span className="skill">HTML & CSS <i className="val">80%</i></span>
                        <div className="progress-bar-wrap">
                          <div className="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style={{ width: "80%" }}></div>
                        </div>
                      </div>

                    </div>

                    <div className="col-md-12">

                      <div className="progress">
                        <span className="skill">SQL <i className="val">80%</i></span>
                        <div className="progress-bar-wrap">
                          <div className="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style={{ width: "80%" }}></div>
                        </div>
                      </div>

                      <div className="progress">
                        <span className="skill">Kotlin <i className="val">30%</i></span>
                        <div className="progress-bar-wrap">
                          <div className="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style={{ width: "30%" }}></div>
                        </div>
                      </div>

                      <div className="progress">
                        <span className="skill">Photography <i class="val">80%</i></span>
                        <div className="progress-bar-wrap">
                          <div className="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style={{ width: "80%" }}></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </Col>

              {/*MAIN TECHNOLOGIES */}
              <Col lg={6}>
                <div className="techdiv" id="techdiv">
                  <h3>TECHNOLOGIES</h3>
                  <p>These are some of the technologies I have used or currently using</p>
                  <section id="skills" className="skills section-bg">
                    <Row className="skills-content">
                      <Col md={6} xs={6}>

                        <div className="progress">
                          <span className="skill">REST API</span>
                          <div className="tech-score">
                            <StarFill size={16} className="filled" />
                            <StarFill size={16} className="filled" />
                            <StarFill className="filled" size={16} />
                          </div>
                        </div>

                        <div className="progress">
                          <span className="skill">Android</span>
                          <div className="tech-score">
                            <StarFill size={16} className="filled" />
                            <StarFill size={16} className="filled" />
                            <Star size={16} />
                          </div>
                        </div>

                        <div className="progress">
                          <span className="skill">Microservices</span>
                          <div className="tech-score">
                            <StarFill size={16} className="filled" />
                            <StarFill size={16} className="filled" />
                            <Star size={16} />
                          </div>
                        </div>

                        <div className="progress">
                          <span className="skill">Docker</span>
                          <div className="tech-score">
                            <StarFill size={16} className="filled" />
                            <StarFill size={16} className="filled" />
                            <Star size={16} />
                          </div>
                        </div>

                      </Col>

                      <Col md={6} xs={6}>

                        <div className="progress">
                          <span className="skill">Spring Boot</span>
                          <div className="tech-score">
                            <StarFill size={16} className="filled" />
                            <StarFill size={16} className="filled" />
                            <StarFill className="filled" size={16} />
                          </div>
                        </div>

                        <div className="progress">
                          <span className="skill">Version Control</span>
                          <div className="tech-score">
                            <StarFill size={16} className="filled" />
                            <StarFill size={16} className="filled" />
                            <Star size={16} />
                          </div>
                        </div>

                        <div className="progress">
                          <span className="skill"><abbr title="Continuous Integration & Continuous Deployment ">CI / CD</abbr></span>
                          <div className="tech-score">
                            <StarFill size={16} className="filled" />
                            <StarFill size={16} className="filled" />
                            <Star size={16} />
                          </div>
                        </div>

                        <div className="progress">
                          <span className="skill">Bootstrap</span>
                          <div className="tech-score">
                            <StarFill size={16} className="filled" />
                            <StarFill size={16} className="filled" />
                            <Star size={16} />
                          </div>
                        </div>
                      </Col>

                    </Row>
                  </section>
                </div>
              </Col>
            </Row>
          </ScrollAnimation>
        </div>


        {/* OTHER TECH & TOOLS*/}
        <div className="othertech">
          <ScrollAnimation animateIn='zoomIn' duration='1.5'>
            <Row>
              <Col lg={6}>
                <div className="techdiv">
                  <h3 style={{ marginBottom: "1rem" }}>OTHER TECH &amp; TOOLS</h3>
                  <div className="tech-logos">
                    <ToolsTagCloud />
                  </div>
                </div>
              </Col>

              {/* LANGUAGES */}
              <Col lg={6}>
                <div className="languages-section">
                  <h3>LANGUAGES</h3>
                  <Row style={{ marginTop: "1.5rem" }}>
                    <Col>
                      English
                    <div className="tech-score" style={{ marginTop: "0.5em" }}>
                        <StarFill size={16} className="filled" />
                        <StarFill size={16} className="filled" />
                        <StarFill className="filled" size={16} />
                      </div>
                    </Col>
                    <Col>
                      French <span style={{ fontSize: "0.6em" }}>(Je l'apprends)</span>
                      <div className="tech-score" style={{ marginTop: "0.5em" }}>
                        <StarFill size={16} className="filled" />
                        <Star size={16} />
                        <Star size={16} />
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            </Row>
          </ScrollAnimation>
        </div>


        {/* FUN FACTS */}
        <div className="funfacts-section">
          <Row>
            <Col lg={6}>
              <ScrollAnimation animateIn='fadeInLeft' animateOut='fadeOutLeft'>
                <Tilt
                  className="tilt-img"
                  tiltMaxAngleX={35}
                  tiltMaxAngleY={35}
                  perspective={900}
                  scale={1.1}
                  transitionSpeed={2000}
                  gyroscope={true}>
                  <img src={Chelsea} width="480" height="480" className="img-fluid avatar" alt="chelsea logo" loading="lazy" />
                </Tilt>
              </ScrollAnimation>
            </Col>

            <Col lg={6}>
              <ScrollAnimation animateIn='zoomIn' duration='1.5'>
                <div className="fun-facts">
                  <h2>Some fun facts about me
                </h2>
                  <br />
                <p className="lastParagraph">
                I love watching a good game of football especially chelsea matches<HeartFill color="#1E90FF" />, I hate losing at fifa, I enjoy listening to different genres of music,
                and travelling to new places. Almost forgot, I love carrying and dropping stuff in the gym too🏋🏽‍♂️
                </p>
                </div>

              </ScrollAnimation>
            </Col>
          </Row>
        </div>

      </Container>
    </div>

  );
}

export default About;