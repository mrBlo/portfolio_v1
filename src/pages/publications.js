import { useRef } from 'react'
import { ArrowDownCircleFill, Link45deg } from 'react-bootstrap-icons'
import { Badge, Button, Container, Row } from 'react-bootstrap'
import ScrollToTop from '../components/scrollToTop'
import ScrollAnimation from 'react-animate-on-scroll';

export default function Publications() {

    const titleRef = useRef()
    const handleScrollDownClick = () => {
        titleRef.current.scrollIntoView({ behavior: 'smooth' })
    }

    return (
        <>
            <section className="pageLead">
                <div className="header">
                    <ScrollAnimation animateIn='flipInY'>
                        <h1>Publications</h1>
                    </ScrollAnimation>
                </div>

                <div className="scroll-div">
                    <div>
                        <ArrowDownCircleFill className="scrollDown" onClick={handleScrollDownClick} color="#18d26e" size={48} />
                    </div>

                </div>
            </section>

            <main>
                <br />
                <span ref={titleRef}></span>
                { /* CONTENT */}
                <section id="pageSection" className="pageSection" style={{ minHeight: "40vh" }}>

                    {/* Scroll Button */}
                    <ScrollToTop />

                    <ScrollAnimation animateIn='zoomIn' duration='1.5'>
                        <div className="softwareProjects">
                            <Container fluid>
                                <Row>
                                    <div className="col-12 col-md-5 offset-md-1">
                                        <div className="mycard mb-3">
                                            <div className="mycard-img" style={{ backgroundImage: "url(https://booksnresearch.com/wp-content/uploads/2020/05/banner1.jpg)" }}>
                                                <div className="mycard-img-overlay d-flex flex-column justify-content-between" style={{ overflowY: "scroll" }}>
                                                    <h4 className="card-title">An Evaluation of Data Erasing Tools</h4>

                                                    <p className="card-text">
                                                        This paper analyzes the efficiency of a number of these tools in performing erasures on...</p>
                                                    <Button className="button btn-success" href="https://commons.erau.edu/jdfsl/vol15/iss1/2/" target="_blank" rel="noopener noreferrer">
                                                        <Link45deg /> Read More...</Button>
                                                    <div className="projectBadges">
                                                        <Badge variant="primary" className="badge">ACADEMIC PAPER</Badge>
                                                        <Badge variant="secondary" className="badge">DIGITAL FORENSICS</Badge>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-12 col-md-5">
                                        <div className="mycard mb-3">
                                            <div className="mycard-img" style={{ backgroundImage: "url(https://cdn.hashnode.com/res/hashnode/image/upload/v1615298615390/s0KV8RBMx.jpeg?w=1600&h=840&fit=crop&crop=entropy&auto=compress)" }}>
                                                <div className="mycard-img-overlay d-flex flex-column justify-content-between" style={{ overflowY: "scroll" }}>
                                                    <h4 className="card-title">Code Smells: Naming</h4>
                                                    <p className="card-text">
                                                        Today's article aims to discuss Naming in relation to good coding practices...</p>
                                                    <Button className="button btn-success" href="https://iafrifa.hashnode.dev/code-smells-naming" target="_blank" rel="noopener noreferrer">
                                                        <Link45deg /> Read More...</Button>
                                                    <div className="projectBadges">
                                                        <Badge variant="success" className="badge">BLOG</Badge>
                                                        <Badge variant="primary" className="badge">CODE SMELLS</Badge>
                                                        <Badge variant="light" className="badge">HASH NODE</Badge>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Row>
                            </Container>
                        </div>

                    </ScrollAnimation>
                </section>

            </main>
        </>
    )
}
