import { Navbar, Nav, Container } from 'react-bootstrap';

const Footer = () => {
    return (
        <div className="row">
        <div className="col-md-12">
        <Navbar className="footer"
             fixed="bottom"
            //  sticky="top" 
            expand='lg' bg='' variant='dark'>
            <Container fluid>
               <Nav className="mr-auto">
               &copy; 2021 Isaac Afrifa  
              </Nav>
            
            </Container>
        </Navbar>
</div>
</div>
      );
}
 
export default Footer;