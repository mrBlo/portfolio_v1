import React from 'react'
import { TagCloud } from 'react-tagcloud'

function ToolsTagCloud() {
    const data = [
        { value: 'Postman', count: 23 },
        { value: 'BitBucket', count: 28 },
        { value: 'Materialize', count: 22 },
        { value: 'OSForensics', count: 24 },
        { value: 'GooglePlayConsole', count: 25 },
        { value: 'Heroku', count: 18 },
        { value: 'Github', count: 23 },
        { value: 'Linux', count: 20 },
        { value: 'NetflixEureka', count: 21 },
        { value: 'Lightroom', count: 17 },
        { value: 'Netbeans', count: 22 },
        { value: 'MySQL', count: 25 },
        { value: 'Netlify', count: 23 },
        { value: 'SpringToolSuite', count: 27 },
        { value: 'VSCode', count: 30 },
        { value: 'Redux', count: 18 },
        { value: 'JUnit', count: 21 },
        { value: 'PHP', count: 11 },
        { value: 'AndroidStudio', count: 30 },
        { value: 'Maven', count: 25 },
      ]

// custom renderer is function which has tag, computed font size and
// color as arguments, and returns react component which represents tag
const customRenderer = (tag, size, color) => (
    <span
      key={tag.value}
      style={{
        animation: 'blinker 3s linear infinite',
        animationDelay: `${Math.random() * 2}s`,
        fontSize: `${size / 2}em`,
        border: `1px solid ${color}`,
        margin: '3px',
        padding: '3px',
        display: 'inline-block',
        color: 'white',
      }} >
      {tag.value}
    </span>
  )

    return (
        <div>
       <TagCloud tags={data} minSize={1} maxSize={3} renderer={customRenderer} />      
        </div>
    )
}

export default ToolsTagCloud
