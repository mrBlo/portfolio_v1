import { Navbar, Nav, Container } from 'react-bootstrap';
import { withRouter } from "react-router";
import { LinkContainer } from "react-router-bootstrap";

const Navigation = () => {
    // const { location } = props;
    // console.log(location);
    return (
        <div className="row">
            <div className="col-md-12">
                <Navbar className="navbar"
                    collapseOnSelect
                    fixed="top"
                    //  sticky="top" 
                    expand='lg' bg='' variant='dark'>
                    <Container fluid>
                        {/* <LinkContainer exact to="/">
                            <Navbar.Brand className="font-weight-bold text-muted">
                                Home
                            </Navbar.Brand>
                        </LinkContainer> */}
                        <Navbar.Toggle aria-controls='basic-navbar-nav' />
                        <Navbar.Collapse id='basic-navbar-nav'>
                            <Nav className="ml-auto" activeKey={window.location.pathname}>

                                <LinkContainer exact to="/">
                                    <Nav.Link>Home</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/about">
                                    <Nav.Link>About</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/resume">
                                    <Nav.Link>Resume</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/portfolio">
                                    <Nav.Link>Portfolio</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/publications">
                                    <Nav.Link>Publications</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/contact">
                                    <Nav.Link>Contact</Nav.Link>
                                </LinkContainer>

                            </Nav>
                        </Navbar.Collapse>

                    </Container>
                </Navbar>
            </div>
        </div>
    );
}

export default withRouter(Navigation);