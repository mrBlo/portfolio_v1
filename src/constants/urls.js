
export const HOME_URL = "/";
export const ABOUT_URL = "/about";
export const RESUME_URL = "/resume";
export const PORTFOLIO_URL = "/portfolio";
export const PUBLICATIONS_URL = "/publications";
export const CONTACT_URL = "/contact";
export const RESUME_DOWNLOAD_URL = "https://drive.google.com/file/d/1Wm0IHAsTntDcW0cTQU-0KmZF7ZHc7GL_/view?usp=sharing";