## PORTFOLIO SITE - V1 ##

### Quick Summary
This is version 1 of my portfolio site which showcases my skills, a little bit about me, and some of my projects.


### Live Demo
[https://imafrifa.netlify.app](https://imafrifa.netlify.app)


## Table of contents
* [App Screenshot](#markdown-header-app-screenshot)
* [General Info](#markdown-header-general-info)
* [Version](#markdown-header-version)
* [Technologies](#markdown-header-technologies)
* [Future Addition](#markdown-header-future-addition)


### App Screenshot ###
![diagram](./app_image.png)


### General Info 
This project is a **ReactJS** application and was bootstrapped with Create React App.


### Version 
+ VERSION: v 1.0


### Technologies 
Project is created using:

* Bootstrap
* React Hook Form
* React Animate - for scroll effect 
* Particles - for particles background
* Parallax Tilt - for tilt effect


### Future Addition
* Call site data from remote API


### Who do I talk to? 
* Repo owner : `isaac.afrifa3@yahoo.com`
